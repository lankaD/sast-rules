// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/ajinabraham/njsscan/blob/master/tests/assets/node_source/true_positives/semantic_grep/database/sql_injection.js
// hash: e7a0a61
var mysql = require('mysql');

const pg = require('pg');
// ruleid:rules_lgpl_javascript_database_rule-node-sqli-injection
connection.query("SELECT * FROM bank_accounts WHERE dob = '" + req.body.dob + "' AND bank_account = '" + req.body.account_number + "'", function (error, results) { });

const sequelize = require('../conn');
router.post('/', function (req, res) {
    // ruleid:rules_lgpl_javascript_database_rule-node-sqli-injection
    var query = 'SELECT * FROM person WHERE id = \'' +
        req.body.input + '\'';
    sequelize.query(query, {
        type: sequelize.QueryTypes.SELECT,
        model: Foo
    })
        .then(function (foo) {
            res.json({ message: person });
        })
        .catch(function (err) {
            res.json({ message: err.toString() });
        });
});

var connection = mysql.createConnection({
    host: 'localhost',
    user: user,
    password: pass,
    database: 'technicalkeeda',
    debug: false,
});
connection.connect();

// ruleid:rules_lgpl_javascript_database_rule-node-sqli-injection
var employeeId = req.foo;
var sql = "SELECT * FROM trn_employee WHERE employee_id = " + employeeId;

connection.query(sql, function (error, results, fields) {
    if (error) {
        throw error;
    }
    console.log(results);
});

connection.connect(function (err) {
    // ruleid:rules_lgpl_javascript_database_rule-node-sqli-injection
    connection.query('SELECT * FROM users WHERE id = ' + req.foo('bar'), (err, res) => { });
});

connection.end();

const pgcon = new pg.Client({ host: host, user: user, password: pass, database: db });
pgcon.connect();
// ruleid:rules_lgpl_javascript_database_rule-node-sqli-injection
var inp = req.foo["id"];
pgcon.query('SELECT * FROM users WHERE id = ' + inp, (err, res) => { });


const pg = require('pg');
const pool = new pg.Pool(config);
function handler(req, res) {
    // ruleid:rules_lgpl_javascript_database_rule-node-sqli-injection
    var query1 = "SELECT FOO,BAR FROM TABLE WHERE CAT='"
        + req.foo.bar + "' ORDER BY FOO";
    pool.query(query1, [], function (err, results) {
    });
}
